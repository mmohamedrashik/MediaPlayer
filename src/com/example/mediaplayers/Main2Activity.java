package com.example.mediaplayers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.IOException;

public class Main2Activity extends Activity {
SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

MediaPlayer mediaPlayer;
    String path;
    VideoView videoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        videoView = (VideoView)findViewById(R.id.videoView);
        path = "sdcard/Download/spinner.3gp";
        final ProgressDialog progressDialog = new ProgressDialog(Main2Activity.this);
        progressDialog.setMessage("LOADING");
        progressDialog.setTitle("HELLO");
        progressDialog.setCancelable(false);
        progressDialog.show();
       try
       {
           MediaController mediaController = new MediaController(Main2Activity.this);
           mediaController.setAnchorView(videoView);

           Uri video = Uri.parse(path);
           videoView.setMediaController(mediaController);

           videoView.setVideoURI(video);
       }catch (Exception e){}
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressDialog.cancel();
                videoView.start();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
